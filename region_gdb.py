'''
Examen Langages pour SIG
Écrire un script python qui permet de générer une geodatabase par région contenant:

une feature représentant les rivières de la région
une feature représentant les routes de la région
une feature des départements de la région
Le script peut prendre en paramètre le nom de la région a exporter ou alors ALL pour exporter l’ensemble.

Author : Miguel NARVAEZ

'''
import os
import errno
import arcpy

class Geoprocess():

    # creation de la GDB
    def createGDB(output_path, name):
        try:
            arcpy.management.CreateFileGDB(output_path, name)
        except:
            os.removedirs(os.path.join(output_path, name))
            arcpy.management.CreateFileGDB(output_path, name)
        print(f'\n\ncréation réussie de la Geodatabase: {name}\n')

    def getFC(directory):
        '''fonction pour generer identifier les features de type .SHP dedans un dossier'''
        # print(f'\nSelecction des features')
        all_files = os.listdir(directory)
        features = [fc for fc in all_files if fc.endswith('.shp')]
        return features

    def feature_name(fc):
        desc = arcpy.Describe(fc)
        return desc.name

    def fc2gdb(fc, gdb):
        '''Fonction pour charger un Feature dans une Geodatabase'''
        try:
            arcpy.conversion.FeatureClassToGeodatabase(fc, gdb)
            fcname= Geoprocess.feature_name(fc)
            # renommer le feature pour garder que des lettres minuscules
            name_lower = fcname.lower()
            arcpy.Rename_management(fc, name_lower, "FeatureClass")
            print(f'\nFeature class {name_lower} chargée dans la geodatabase {gdb}')
        except:
            print(arcpy.GetMessages())

    def createFC(fc, fc_name, gdb):
        '''Load a feature class to the GDB'''
        try:
            arcpy.env.workspace = gdb
            arcpy.CopyFeatures_management(
                fc,
                fc_name)
        except:
            print(arcpy.GetMessages())

    def clip(fc, sel_region, outgdb):
        print(f'Clipper des couches {fc}')
        fc_name = Geoprocess.feature_name(fc)
        out = os.path.join(outgdb, fc_name)
        try:
            arcpy.gapro.ClipLayer(
                fc,
                sel_region,
                out)
        except:
            print(arcpy.GetMessages())

    def selByLocation(fc, sel_region, outgdb):
        '''Select by location'''
        try:
            sel_fc = arcpy.management.SelectLayerByLocation(
                fc,
                "WITHIN",
                sel_region,
                None,
                "NEW_SELECTION",
                "NOT_INVERT")
            # Écrire les entités sélectionnées dans une nouvelle classe d'entités
            fcname = Geoprocess.feature_name(fc)
            Geoprocess.createFC(sel_fc, f"{fcname}", outgdb)
        except:
            print(arcpy.GetMessages())

    def allRegions(outdir):
        arcpy.env.workspace = os.path.abspath('.\gdb_parent.gdb')
        with arcpy.da.SearchCursor("region","INSEE_REG") as cursor:
            try:
                for row in cursor:
                    insee_reg = row[0]
                    Geoprocess.insee_reg(insee_reg)
            except:
                    print(arcpy.GetMessages())

    def insee_reg(insee_reg):
        arcpy.env.workspace = os.path.abspath('.\gdb_parent.gdb')
        arcpy.env.overwriteOutput = True
        outdir = os.path.abspath('./resultats')
        # create GDB
        Geoprocess.createGDB(outdir, f'region{insee_reg}')
        outgdb = os.path.abspath(os.path.join(outdir, f'region{insee_reg}.gdb'))

        print(f'\nla région sélectionnée est: {insee_reg}')
        try:
            arcpy.env.outputCoordinateSystem = arcpy.SpatialReference(4326)
            region = insee_reg
            query = """ "INSEE_REG" = '%s'"""%region
            sel_region = arcpy.SelectLayerByAttribute_management("region", "NEW_SELECTION", query, None)
            # Écrire les entités sélectionnées dans une nouvelle classe d'entités
            Geoprocess.createFC(sel_region, f"region", outgdb)

            # Geoprocess.region_intersects(sel_region)
            arcpy.env.workspace = os.path.abspath('.\gdb_parent.gdb')
            fclist = ['route', 'riviere', 'departement']
            for layer in fclist:
                desc = arcpy.Describe(layer)
                fc_type = desc.shapeType
                # Definir traitment par type de geometrie
                if fc_type == "Polygon":
                    # select by location
                    Geoprocess.selByLocation(layer,sel_region,outgdb)
                else:
                    Geoprocess.clip(layer, sel_region, outgdb)
        except:
            print(arcpy.GetMessages())


    def main_func(insee_reg):
        print(f'\nWorking directory is: {os.path.abspath(".")}')

        # indiquer l'emplacement du dossier avec les données de départ departement, insee_reg, riviere, route
        data_dir = os.path.abspath('.\data')

        # création du dossier de résultats
        output_dir = 'resultats'

        # indiquer l'espace de travail arcpy, dossier courante
        work_space = os.path.abspath('.')

        arcpy.env.workspace = work_space
        arcpy.env.overwriteOutput = True

        try:
            os.makedirs(output_dir, exist_ok=True)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

        # création d'une géodatabase parent avec les features principaux
        Geoprocess.createGDB(work_space, 'gdb_parent')

        # charger les features directement accessibles dans la geodatabase
        fc_list = Geoprocess.getFC(os.path.abspath(data_dir))

        for fc in fc_list:
            fc_path = os.path.abspath(os.path.join(data_dir, fc))
            Geoprocess.fc2gdb(fc_path, 'gdb_parent.gdb')

        # traitement des dossiers; riviere et routes
        list_files = os.listdir(os.path.abspath(data_dir))
        fc_dirs = [dir for dir in list_files if os.path.isdir(os.path.join(data_dir, dir))]

        # boucler dedans chaque dossier et charger les features dans la base de données parent
        for fc_dir in fc_dirs:
            fc_dir_path = os.path.join(data_dir, fc_dir)
            # lister les shapefiles
            features = Geoprocess.getFC(fc_dir_path)

            # convert features vers GDB
            for fc in features:
                fc_p = os.path.abspath(os.path.join(fc_dir_path, fc))
                Geoprocess.fc2gdb(fc_p, 'gdb_parent.gdb')

        if insee_reg != 'ALL':
            # selectioner la region indiquée par l'utilisateur
            Geoprocess.insee_reg(insee_reg)

        else:
            # Boucler par tous las regions
            Geoprocess.allRegions(output_dir)


if __name__ == "__main__":
    insee_reg = arcpy.GetParameterAsText(0)
    # insee_reg = '11'
    # insee_reg = 'ALL'

    Geoprocess.main_func(insee_reg)
    print(f'\nExécution finalisée')
